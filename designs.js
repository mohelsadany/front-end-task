// Select color input
// Select size input

var pickColor = document.getElementById('colorPicker').value

// When size is submitted by the user, call makeGrid()
function makeGrid(){
  this.event.preventDefault()
  // Your code goes here!
  var table = document.getElementById('pixelCanvas');
  var row = document.getElementById('inputHeight').value;
  var col = document.getElementById('inputWidth').value;

  // reset the table
  table.innerHTML = ''

  for(let i = 0; i < row; i++) {
    var tr = document.createElement('tr');

    for(let j = 0; j < col; j++) {
      var td = document.createElement('td');
      var text  = document.createTextNode("");
      td.appendChild(text);
      tr.appendChild(td);
    }

    table.appendChild(tr)
  }

  prepareTableCellsForBgToggle()
}

var toggleCellBackground = function (e) {
  var bg = e.target.style.backgroundColor;

  if (bg == pickColor) {
    e.target.style.backgroundColor = '';
  } else {
    e.target.style.backgroundColor = pickColor;   
  }
}

var updatePickedColor = function() {
  pickColor = document.getElementById('colorPicker').value
}

var prepareTableCellsForBgToggle = function (e) {
  updatePickedColor()

  var cells = document.getElementsByTagName("td")

  for (var i = 0 ; i < cells.length ; i++) {
    cells[i].onclick = function(event) {
      toggleCellBackground(event);
    }
  }
}












  


